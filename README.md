# MirrorRepo

1. Create new project in Gitlab
2. Import project by URL
3. Paste link from original git repo
4. Add project name
5. Choose private type of project
6. Create project
7. Create branch fork
8. Add gitlab-runner
9. Add Deploy token in settings (read/write access)
10. Add gitlab-ci file
